                    comandos
Iniciar servidor con el comando: php artisan serve
Generar test con el comando: php artisan make:test NombreDeLaPruebaTest
Iniciar test con el comando: vendor/bin/phpunit
Generar un nuevo controlador: php artisan make:controller UserController
Para utilizar Blade debemos llamar anuestro archivo con la extención .blade.php

                                        DOCUMENTACIÓN
-Librerias:
Se utilizaron las librerias que trae por defecto al crear el proyecto con Composer
    -phpunit = Fue utilizada para realizar las pruebas del sistema.
    -php = Libreria estandar para funciones basicas.
    -fideloper/proxy = Permite la correcta generación de URL, redireccionamiento.
    -laravel/framework = Laravel es un framework de código abierto para desarrollar aplicaciones y servicios web.
    -laravel/tinker = Nos permite interactuar con toda la aplicación que estemos creando a través de la linea de comandos.
    -beyondcode/laravel-dump-server = Recopila todas las salidas de llamadas, para que no interfiera con las respuestas HTTP / API.
    -filp/whoops = Es un framework de manejo de errores para PHP.
    -fzaninotto/faker = Es una biblioteca PHP que genera datos falsos.
    -mockery/mockery = Es un marco de objetos ficticios php simple pero flexible para su uso en pruebas unitarias con PHPUnit.
    -nunomaduro/collision = Diseñado para darle un hermoso informe de errores al interactuar con su aplicación a través de la línea de         comandos.

    Aunque aquí menciono todas las librerias que tiene el proyecto yo solo utilice 4, phpunit,php,laravel/framework y filp/whoops

-Variables:
Se utilizaron algunas variables a la hora de crear el CRUD, la mayoria estando en el controlador del mismo
    -title = utilizada para la creacion de columnas dentro de la tabla que se iba a migrar
    -id = utilizada para identificar los objetos
    -users = utilizado para almacenar valores
    -name = utilizado para almacenar todos los objetos de su mismo tipo
    -nickname = utilizado para guardar elobjeto

-Funciones:
Se utilizaron funciones, estando todas en el controller del CRUD
    -index = En esta funcion nos retorna lavista y la lista de objetos guardados
    -store = Esta funcion es utilizada para guardar la nota
    -edit = Esta funcion es utilizada para encontrar el id de la nota
    -update = Utilizada para actualizar la nota
    -destroy = Utilizada para destruir la nota